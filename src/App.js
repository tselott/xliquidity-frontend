import { Route, Routes } from 'react-router-dom';
import './App.css';
import PrimarySearchAppBar from './components/AppBar';
import Profile from './components/Profile';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route exact path='/' element={<PrimarySearchAppBar/>} />
        <Route exact path='/profile' element={<Profile/>} />
      </Routes>
    </div>
  );
}

export default App;
