import React, { Suspense, useState, useEffect } from "react";
import { useGLTF, OrbitControls } from "@react-three/drei";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";
import "@google/model-viewer/dist/model-viewer";

const useStyles = makeStyles((theme) => ({
  threeD: {
    margin: "0 auto",
    [theme.breakpoints.up("sm")]: {
      width: "60%",

    },
  },
  card:{
    [theme.breakpoints.down("sm")]: {
      marginLeft:"-70px"

    },
  }
}));

export const ThreeDBanner = (props) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  
  function Model() {
    const { scene } = useGLTF("/assets/3D/SBM Credit CC itr1.glb");
    console.log("scene: ", scene);
    setLoading(false);
    return <primitive object={scene} />;
  }

  return (
    <Box className={classes.threeD}>
      {/* {loading && 
        <Loader />
      } */}
      <Grid container>
        <Grid item xs={12}>
          <Box className={classes.card} style={{ height: "516px", width:"500px" ,backgroundColor: "white", marginLeft:"-50px"}}>
            {/* <Canvas camera={{ position: [1, 9, 3], fov: 0.5 }}>
              <ambientLight intensity={0.5} />
              <directionalLight position={[0, 1, 0]} intensity={2} />
              <directionalLight position={[0, 200, 500]} intensity={2} />
              <directionalLight position={[50, 100, 0]} intensity={2} />
              <directionalLight position={[0, 100, -100]} intensity={2} />
              <directionalLight position={[-500, 300, 500]} intensity={2} />
              <spotLight
                angle={0.1}
                position={[10, -10, -10]}
                castShadow
                penumbra={1}
              />
              <pointLight position={[-10, -10, -10]} intensity={1} />
              <Suspense fallback={null}>
                <Model />
              </Suspense>
              <OrbitControls />
            </Canvas> */}
            <model-viewer
              src="/assets/3D/SBM Credit CC itr1.glb"
              ios-src=""
              // poster="https://cdn.glitch.com/36cb8393-65c6-408d-a538-055ada20431b%2Fposter-astronaut.png?v=1599079951717"
              alt="A 3D model of an astronaut"
              shadow-intensity="1"
              camera-controls
              auto-rotate
              ar
              style={{height:"100%", width:"100%"}}
            ></model-viewer>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
