import {
  Container,
  Tab,
  TableBody,
  TableContainer,
  TableRow,
  Table,
  TableHead,
  TableCell,
  makeStyles,
  Grid,
  Typography,
  Button,
  Modal,
  Fade,
  Backdrop,
  Box,
  TextField,
  IconButton,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Snackbar,
} from "@material-ui/core";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import QrCodeIcon from "@mui/icons-material/QrCode";
import WarningOutlinedIcon from "@mui/icons-material/WarningOutlined";
import { Alert } from "@mui/material";
import axios from "axios";
import React from "react";
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "40px",
  },
  tableCell: {
    padding: "15px",
  },
  img: {
    width: "32px",
    height: "32px",
    marginRight: "10px",
  },
  text1: {
    marginTop: "6px",
  },
  text2: {
    color: "#4A5056",
    fontWeight: "500",
    fontSize: "1.14286rem",
  },
  text3: {
    color: "#83888C",
    fontSize: "1rem",
    fontWeight: "400",
  },
  btn1: {
    background: "#1E4DD8",
    borderColor: "#1E4DD8",
    color: "white",
    height: "40px",
    width: "137px",
    fontSize: "14px",
    padding: "0 24px",
    marginRight: "10px",
    textTransform: "none",
    "&:hover": {
      background: "#08298a",
      color: "white",
    },
  },
  btn2: {
    background: "#F4F6FD",
    borderColor: "#F4F6FD",
    color: "#1E4DD8",
    height: "40px",
    width: "137px",
    fontSize: "14px",
    padding: "0 24px",
    textTransform: "none",
    "&:hover": {
      background: "#F4F6FD",
      color: "#1E4DD8",
    },
  },
  imgMini: {
    width: "25px",
    height: "25px",
    margin: "0px 7px",
  },
  box1: {
    display: "flex",
  },
  box2: {
    marginTop: "25px",
  },
  textInput: {
    width: "100%",
    marginTop: "20px",
  },
  box3: {
    background: "#FFFAE6",
    padding: "0.57143rem 1.14286rem",
    borderRadius: "0.28571rem",
    marginTop: "15px",
    display: "flex",
    borderLeft: "4px solid #FFCC01",
  },
  text4: {
    paddingLeft: "10px",
    fontSize: "1rem",
    fontWeight: "500",
  },
  btn3: {
    background: "#F4F6FD",
    borderColor: "#F4F6FD",
    color: "#1E4DD8",
    height: "40px",
    width: "100%",
    fontSize: "14px",
    padding: "0 24px",
    textTransform: "none",
    marginTop: "20px",
    "&:hover": {
      background: "#F4F6FD",
      color: "#1E4DD8",
    },
  },
}));
const style = {
  position: "absolute",
  top: "30%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "800px",
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 3,
  borderRadius: "7px",
};
const CryptoTable = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [withdrawMethod, setWithdrawMethod] = React.useState("to_blockchain");
  const [wallet, setWallet] = React.useState("");
  const [amount, setAmount] = React.useState("");
  const [toBank, setToBank] = React.useState("");
  const [showAlert, setShowAlert] = React.useState(false);
  const [showBalanceAlert, setShowBalanceAlert] = React.useState(false);
  
  const [userWallet, setUserWallet] = React.useState([])

  const [isButtonLoading, setIsButtonLoading] = React.useState(false);
  const handleChange = (e) => {
    setWithdrawMethod(e.target.value);
  };
  const handleWalletChange = (e) => {
    setWallet(e.target.value);
  };

  const handleCloseAlert = () => {
    setShowAlert(false);
  }
  const handleCloseBalanceAlert = () => {
    setShowBalanceAlert(false);
  }

  const fetchUserWallets = (user_id) => {
    const headers = {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    };
    axios
      .get(
        "http://localhost:8080/get_wallet_ids/"+user_id,
        { headers: headers }
      )
      .then((res) => {
        setUserWallet(res.data)
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  React.useEffect(()=>{
    fetchUserWallets(2)
  },[])

  const handleWithdraw = (e) => {
    setIsButtonLoading(true);
    const headers = {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    };
    console.log(wallet);
    axios
      .post(
        "http://localhost:8080/send_fund",
        {
          user_id: 1,
          from_wallet: wallet,
          to_bank: toBank,
          amount: amount,
        },
        { headers: headers }
      )
      .then((res) => {
        setIsButtonLoading(false);
        setShowAlert(true);
        setWallet("")
        setAmount("")
        setToBank("")
        setOpen(false);
        console.log(res);
      })
      .catch((err) => {
        if(err.response.data.message === 'Wallet does not have sufficient ballance'){
          setShowBalanceAlert(true)
        }
        setIsButtonLoading(false);
      });
  };

  return (
    <Container className={classes.root}>
      <Snackbar open={showAlert} autoHideDuration={6000} onClose={handleCloseAlert}
        anchorOrigin={{vertical:'top',horizontal:'center'}}
        key={'top center'}
      >
        <Alert onClose={handleCloseAlert} severity="success" sx={{ width: "100%" }}>
          The withdraw was successfully sent 
        </Alert>
      </Snackbar>
      <Snackbar open={showBalanceAlert} autoHideDuration={6000} onClose={handleCloseBalanceAlert}
        anchorOrigin={{vertical:'top',horizontal:'center'}}
        key={'top center'}
      >
        <Alert onClose={handleCloseBalanceAlert} severity="error" sx={{ width: "100%" }}>
          Wallet doesn't have sufficient balance
        </Alert>
      </Snackbar>
      <Table
        sx={{ minWidth: 650, tableLayout: "auto" }}
        style={{ border: "1px solid rgba(224, 224, 224, 1)" }}
      >
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableCell}>Calculator</TableCell>
            <TableCell className={classes.tableCell} align="right">
              Balance
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              Locked To Card Limit
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              % Limit Conversion
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              Current Card Limit Contribution
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              % Age (APR)
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button
                className={classes.btn1}
                onClick={handleOpen}
                variant="contained"
              >
                Top Up
              </Button>
              <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                  timeout: 500,
                }}
              >
                <Fade in={open}>
                  <Box sx={style}>
                    <Box className={classes.box1}>
                      <Typography
                        style={{ fontSize: "1.42857rem", fontWeight: "500" }}
                      >
                        Withdraw
                      </Typography>
                      <img
                        className={classes.imgMini}
                        src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                      />
                      <Typography
                        style={{ fontSize: "1.42857rem", fontWeight: "500" }}
                      >
                        Bitcoin
                      </Typography>
                    </Box>
                    <FormControl fullWidth style={{ marginBottom: "30px",marginTop: "15px"}}>
                      <InputLabel id="demo-simple-select-label">
                        Choose withdrawal method
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={withdrawMethod}
                        label="Choose withdrawal method"
                        onChange={handleChange}
                      >
                        <MenuItem value={"to_blockchain"}>
                          Withdraw to Blockchain
                        </MenuItem>
                        <MenuItem value={"to_bank"}>Withdraw to Bank</MenuItem>
                      </Select>
                    </FormControl>
                    {withdrawMethod === "to_blockchain" ? (
                      <>
                        <Box className={classes.box2}>
                          <IconButton aria-label="delete">
                            <ContentCopyIcon />
                          </IconButton>
                          <IconButton aria-label="delete">
                            <QrCodeIcon />
                          </IconButton>
                        </Box>
                        <Box className={classes.box3}>
                          <WarningOutlinedIcon style={{ color: "#FFCC01" }} />
                          <Typography className={classes.text4}>
                            Send only Bitcoin (BTC) to this address using the
                            Bitcoin (native) network.
                          </Typography>
                        </Box>
                      </>
                    ) : (
                      <>
                        <Box className={classes.box2}>
                          <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">
                              Choose Wallet
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={wallet}
                              label="Choose Wallet"
                              onChange={handleWalletChange}
                            >
                              {userWallet && userWallet.map(data => (
                                <MenuItem key={data.id} value={data.wallet}>
                                  {data.wallet}
                                </MenuItem>

                              ))}
                            </Select>
                          </FormControl>
                          <TextField
                            value={toBank}
                            className={classes.textInput}
                            id="outlined-basic"
                            label="To Bank Address"
                            onChange={(e) => setToBank(e.target.value)}
                            variant="outlined"
                          />
                          <TextField
                            value={amount}
                            className={classes.textInput}
                            id="outlined-basic"
                            label="Amount"
                            onChange={(e) => setAmount(e.target.value)}
                            variant="outlined"
                          />
                          <FormControl fullWidth>
                            <Button
                              onClick={handleWithdraw}
                              className={classes.btn3}
                              variant="contained"
                              disabled={isButtonLoading}
                            >
                              Withdraw
                            </Button>
                          </FormControl>
                        </Box>
                      </>
                    )}
                  </Box>
                </Fade>
              </Modal>
              <Button
                className={classes.btn2}
                variant="contained"
                onClick={handleOpen}
              >
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Top Up
              </Button>
              <Button className={classes.btn2} variant="contained">
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Top Up
              </Button>
              <Button className={classes.btn2} variant="contained">
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Top Up
              </Button>
              <Button className={classes.btn2} variant="contained">
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Top Up
              </Button>
              <Button className={classes.btn2} variant="contained">
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              <Grid container spacing={2} justifyContent="left">
                <Grid item xs={3}>
                  <img
                    className={classes.img}
                    src="https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Bitcoin-BTC-icon.png"
                  />
                </Grid>
                <Grid item xs={9}>
                  <Typography className={classes.text1}>Bitcoin</Typography>
                </Grid>
              </Grid>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text2}>BTC 0.00000000</Typography>
              <Typography className={classes.text3}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Typography className={classes.text1}>$0.00</Typography>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Top Up
              </Button>
              <Button className={classes.btn2} variant="contained">
                Withdraw
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} align="right">
              <Button className={classes.btn1} variant="contained">
                Activate
              </Button>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Container>
  );
};

export default CryptoTable;
