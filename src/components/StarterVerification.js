import { Box, Container, Grid, makeStyles, Typography } from "@material-ui/core"
import { CheckCircleOutline as CheckCircleIcon } from "@material-ui/icons";
const useStyles = makeStyles((theme) => ({
    root: {
    },
    text1:{
        fontSize:"1.42857rem",
        fontWeight:"500",
        color:"#252A2F",
        marginLeft:"40px",
        marginTop:"20px"
    },
    text2:{
        fontSize:"1.14286rem",
        textAlign:"left",
        padding:"0 0 1.14286rem"

    },
    text3:{
        fontSize:"1.14286rem",
        fontWeight:"500",
        borderLeft:"0.42857rem solid #00A76A",
        height:"58px",
        paddingLeft:"1.71429rem",
        display:"flex",
        alignItems:"center",
        justifyContent:"flex-start",
        borderTopLeftRadius:"5px",
        borderBottomLeftRadius:"5px",
    },
    text4:{
        fontSize:"1.14286rem",
        fontWeight:"400",
        height:"58px",
        display:"flex",
        alignItems:"center",
        justifyContent:"flex-start",
        color:"#252A2F"
    },
    text5:{
        fontSize:"1.14286rem",
        fontWeight:"400",
        height:"58px",
        display:"flex",
        alignItems:"center",
        justifyContent:"flex-start",
        color:"#252A2F"
    },
    text6:{
        fontSize:"1.14286rem",
        fontWeight:"500",
        borderLeft:"0.42857rem solid #1E4DD8",
        height:"58px",
        paddingLeft:"1.71429rem",
        display:"flex",
        alignItems:"center",
        justifyContent:"flex-start",
        borderTopLeftRadius:"5px",
        borderBottomLeftRadius:"5px"
    },
    text7:{
        color:"#4A5056",
        fontWeight:"400",
        fontSize:"1.14286rem",
        marginLeft:"40px",
    },
    icon:{
        fontSize:"4rem",
        color:"white",
        height:"4.57143rem",
        width:"4.57143rem",
        backgroundColor:"#4BD359",
        borderRadius:"2.28571rem",
    },
    box1:{
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
        marginTop:"30px"
    },
    box2:{
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
        marginTop:"10px"
    },
    text8:{
        fontSize:"2rem",
        fontWeight:"500",
        textAlign:"center"
    }

    
  }));
const StarterVerification = () =>  {
    const classes = useStyles();

    return (
        <Container>
            <Typography className={classes.text1}>Cryptocurrency Limits</Typography>
            <Grid container style={{marginTop:"30px",marginLeft:"40px"}}>
                <Grid item xs={4}></Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text2}>Top Up</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text2}>Withdraw</Typography>
                </Grid>
            </Grid>
            <Grid container style={{marginTop:"5px",marginLeft:"40px",}}>
                <Grid item xs={4}>
                    <Typography className={classes.text3}>Savings Wallet</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text4}>unlimited</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text5}>$20,000.00/day*</Typography>
                </Grid>
            </Grid>
            <Grid container style={{marginTop:"2px",marginLeft:"40px"}}>
                <Grid item xs={4}>
                    <Typography className={classes.text6}>Credit Line Wallet</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text4}>unlimited</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text5}>$20,000.00/day*</Typography>
                </Grid>
            </Grid>
            <Typography style={{marginTop:"40px"}} className={classes.text1}>Bank Transfer Limits</Typography>
            <Typography style={{}} className={classes.text7}>Available in 45+ currencies</Typography>
            <Grid container style={{marginTop:"30px",marginLeft:"40px"}}>
                <Grid item xs={4}></Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text2}>Top Up</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text2}>Withdraw</Typography>
                </Grid>
            </Grid>
            <Grid container style={{marginTop:"2px",marginLeft:"40px",}}>
                <Grid item xs={4}>
                    <Typography className={classes.text3}>Savings Wallet</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text4}>unlimited</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text5}>$20,000.00/day*</Typography>
                </Grid>
            </Grid>
            <Grid container style={{marginTop:"2px",marginLeft:"40px"}}>
                <Grid item xs={4}>
                    <Typography className={classes.text6}>Credit Line Wallet</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text4}>unlimited</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography className={classes.text5}>$20,000.00/day*</Typography>
                </Grid>
            </Grid>
            <Box className={classes.box1}>
                <CheckCircleIcon className={classes.icon} />
            </Box>
            <Box className={classes.box2}>
                <Typography style={{marginTop:""}} className={classes.text8}>You are successfully verified!</Typography>
            </Box>
            
        </Container>
    )
}

export default StarterVerification