import {
  Badge,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  AppBar,
  makeStyles,
  Container,
  Tabs,
  Tab,
  Grid,

} from "@material-ui/core";
import {
  AccountCircle,
  AccountCircleOutlined,
  Notifications as NotificationsIcon,
  MoreVert as MoreIcon,
  Menu as MenuIcon,
  Mail as MailIcon,
} from "@material-ui/icons";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import PersonIcon from '@mui/icons-material/Person';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import * as React from "react";
import MostPart from "./MostPart";
import Profile from "./Profile";
const useStyles = makeStyles((theme) => ({
  root: {},
  appBar: {
    background: "white",
    color: "#83888C",
  },
}));
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div {...other}>
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}
const PrimarySearchAppBar = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const classes = useStyles();
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          size="large"
          aria-label="show 17 new notifications"
          color="inherit"
        >
          <Badge badgeContent={17} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
  
  return (
    <nav className={classes.root}>
      <AppBar position="static" color="default">
        <Container>
          <Toolbar>
            <Grid justify={"space-between"} container>
              <Grid xs={1} item>
                <Typography
                  variant="h6"
                  component="div"
                  sx={{ display: { xs: "none", sm: "block" } }}
                  style={{ marginTop: "8px" }}
                >
                  XLiquidity
                </Typography>
              </Grid>
              <Grid xs={7} item>
                <Grid container justify={"center"}>
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    textColor="primary"
                    indicatorColor="primary"
                    aria-label="primary tabs"
                  >
                    <Tab label={<div style={{display:"flex", textTransform:"none"}}> Account <AccountBoxIcon style={{marginLeft:"5px"}} /></div>} />
                    <Tab label={<div style={{display:"flex", textTransform:"none"}}> Transaction <AccountBalanceWalletIcon style={{marginLeft:"5px"}} /></div>} />
                    <Tab label={<div style={{display:"flex", textTransform:"none"}}> Profile <PersonIcon style={{marginLeft:"5px"}} /></div>} />
                    <Tab label={<div style={{display:"flex", textTransform:"none"}}> Exchange <CurrencyExchangeIcon style={{marginLeft:"5px"}} /></div>} />
                  </Tabs>
                </Grid>
              </Grid>
              <Box>
                <IconButton
                  size="large"
                  aria-label="show 4 new mails"
                  color="inherit"
                >
                  <Badge badgeContent={4} color="error">
                    <MailIcon />
                  </Badge>
                </IconButton>
                <IconButton
                  size="large"
                  aria-label="show 17 new notifications"
                  color="inherit"
                >
                  <Badge badgeContent={17} color="error">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
                <IconButton
                  size="large"
                  edge="end"
                  aria-label="account of current user"
                  aria-controls={menuId}
                  aria-haspopup="true"
                  onClick={handleProfileMenuOpen}
                  color="inherit"
                >
                  <AccountCircleOutlined />
                </IconButton>
              </Box>
            </Grid>
          </Toolbar>
        </Container>
      </AppBar>
      <TabPanel value={value} index={0}>
        <MostPart/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Transaction Page
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Profile />
      </TabPanel>
      <TabPanel value={value} index={3}>
        Exchange Page
      </TabPanel>
      {renderMobileMenu}
      {renderMenu}
    </nav>
  );
};
export default PrimarySearchAppBar;
