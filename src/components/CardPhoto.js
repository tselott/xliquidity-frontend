import { Button, Container, Grid, makeStyles } from "@material-ui/core";
import React from "react";
import { ThreeDBanner } from "./ThreeDBanner";
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "40px",
  },
  btn1: {
    background: "#1E4DD8",
    borderColor: "#1E4DD8",
    color: "white",
    height: "40px",
    width: "137px",
    fontSize: "14px",
    padding: "0 24px",
    textTransform: "none",
    "&:hover": {    
        background: "#08298a",
        color: "white",
    },
    marginLeft:"50%",
    marginTop:"30px"
  },
}));
const CardPhoto = (prop) => {
  const classes = useStyles();

  return (
      <Container>
        <Grid container className={classes.root} justifyContent="center">
        <Grid item xs={2}></Grid>
        <Grid item xs={8}>
            <ThreeDBanner />
        </Grid>
        </Grid>
        <Button className={classes.btn1} variant="contained">
            Activate
        </Button>
      </Container>
  );
};

export default CardPhoto;
