import React from "react";
import CardPhoto from "./CardPhoto";
import CryptoTable from "./CryptoTable";
import InfoCards from "./InfoCards";

const MostPart = () => {
  return (
    <>
      <CardPhoto />
      <InfoCards />
      <CryptoTable />
    </>
  );
};
export default MostPart;
