import { Box, Card, Container, Grid, makeStyles, Typography } from "@material-ui/core";
import React from "react";
const useStyles = makeStyles((theme) => ({
    root: {
        marginTop:"40px"
    },
    text1:{
        marginLeft:"30px",
        marginTop:"20px",
        color:"#83888C",
        fontSize:"1.14em",
        fontWeight:"500"
    },
    text2:{
        marginTop:"110px",
        marginLeft:"30px",
        fontSize:"2em",
        fontWeight:"500",

    },
    text3:{
        fontSize:"2em",
        fontWeight:"500",
        marginLeft:"30px",
    },
    text4:{
        color:"#83888C",
        fontSize:"1.14em",
        fontWeight:"500",
        marginLeft:"30px",
        marginTop:"30px"
    },
    text5:{
        color:"#83888C",
        fontSize:"1.14em",
        fontWeight:"500",
        marginLeft:"30px",
        marginTop:"20px"
    },
    text6:{
        fontSize:"2em",
        fontWeight:"500",
        marginLeft:"30px",
    },
    text7:{
        color:"#252A2F",
        fontSize: "1.42857rem",
        fontWeight: "400",
        
    },
    text8:{
        color:"#1E4DD8",
        fontWeight:"700"
    },
    box1:{
        textAlign:"center",
        marginTop:"40px",
        backgroundColor:"#F6F8FB",
        padding:"2.28571rem",
        borderRadius:"2.8px"
    }
    
  }));
const InfoCards = () => {
    const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Grid container spacing={4}>
        <Grid item xs={4}>
          <Card elevation={2}  style={{height:"207px"}}>
                <Box>
                    <Typography className={classes.text5}>Portfolio Balance</Typography>
                    <Typography className={classes.text3}>$0.00</Typography>
                </Box>
                <Box>
                    <Typography className={classes.text4}>Card Balance</Typography>
                    <Typography className={classes.text6}>$0.00</Typography>
                </Box>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={2}  style={{height:"207px"}}>
                <Box>
                    <Typography className={classes.text5}>Total Locked Balance</Typography>
                    <Typography className={classes.text3}>$0.00</Typography>
                </Box>
                <Box>
                    <Typography className={classes.text4}>Total Rewards Earned</Typography>
                    <Typography className={classes.text6}>$0.00</Typography>
                </Box>
          </Card>
        </Grid>
        <Grid item xs={4}>
        <Card elevation={2}  style={{height:"207px"}}>
                <Box>
                    <Typography className={classes.text5}>Balance rewards</Typography>
                    <Typography className={classes.text3}>$0.00</Typography>
                </Box>
                <Box>
                    <Typography className={classes.text4}>Credited Rewards</Typography>
                    <Typography className={classes.text6}>$0.00</Typography>
                </Box>
          </Card>
        </Grid>
      </Grid>
      <Box className={classes.box1} justifyContent={"center"}>
        <Typography className={classes.text7}>Estimated Value of Crypto: <span className={classes.text8}>$0.00</span></Typography>    
        <Typography className={classes.text7}>Total card limit as of now: <span className={classes.text8}>$0.00</span></Typography>    
      </Box>
    </Container>
  );
};

export default InfoCards;
