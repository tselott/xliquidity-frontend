import React from "react";
import {
    Box,
  Container,
  Grid,
  makeStyles,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import StarterVerification from "./StarterVerification";
const useStyles = makeStyles((theme) => ({
  root: {},
  menus: {
    color: "#83888C",
    fontSize: "1.14286rem",
    fontWeight: "500",
    display: "flex",
    alignItems: "center",
    padding: "1.0rem 1.71429rem",
    justifyContent: "flex-start",
    borderLeftWidth: "0.21429rem",
    borderColor: "#E6E9EC",
    boxSizing: "border-box",
    borderLeftStyle: "solid",
    cursor:"pointer"
  },
  active: {
    color: "#252A2F",
    fontSize: "1.14286rem",
    fontWeight: "500",
    display: "flex",
    alignItems: "center",
    padding: "1.0rem 1.71429rem",
    justifyContent: "flex-start",
    borderLeftWidth: "0.21429rem",
    borderColor: "#3A8EF6",
    boxSizing: "border-box",
    borderLeftStyle: "solid",
    cursor:"pointer"
  },
  tabs:{
      display:"flex",
      alignItems:"center",
      justifyContent:"center"
  }
}));
const Profile = () => {
    const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div {...other}>
        {value === index && <Box p={3}>{children}</Box>}
      </div>
    );
  }
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <Grid container>
        <Grid item xs={2}>
          <Typography className={classes.active}>Verification Level</Typography>
          <Typography className={classes.menus}>Loyality Level</Typography>
          <Typography className={classes.menus}>Refer a Friend</Typography>
          <Typography className={classes.menus}>Setting</Typography>
        </Grid>
        <Grid item xs={10}>
          <Grid container className={classes.tabs} justify={"center"}>
            <Tabs
              value={value}
              onChange={handleChange}
              textColor="primary"
              indicatorColor="primary"
              aria-label="primary tabs"
            >
              <Tab label="Starter Verification" />
              <Tab label="Basic Verification" />
              <Tab label="Advanced Verification" />
            </Tabs>
          </Grid>
            <TabPanel value={value} index={0}>
                <StarterVerification />
            </TabPanel>
            <TabPanel value={value} index={1}>
                Basic Verification
            </TabPanel>
            <TabPanel value={value} index={2}>
                Advanced Verification
            </TabPanel>
        </Grid>
      </Grid>
    </Container>
  );
};
export default Profile;
